#include <iostream>
#include <limits>
#include <map>
#include <string>
#include <tuple>
#include <unordered_map>
#include <vector>

template <class T>
void PrintVector(const std::vector<T>& v) {
    for (const auto& element : v) {
        std::cout << element << ' ';
    }
    std::cout << '\n';
}

template <class Key, class Value>
void PrintMap(const std::map<Key, Value>& map) {
    for (const auto& [k, v] : map) {
        std::cout << k << ' ' << v << '\n';
    }
}

void InfiniteRecursion() {
    return InfiniteRecursion();
}

struct Student {
    std::string name;
    std::string surname;
    int age;
};

bool CompareByName(const Student& lhs, const Student& rhs) {
    return std::tie(lhs.surname, lhs.name, lhs.age) < std::tie(rhs.surname, rhs.name, rhs.age);
}

int main() {
    Student student {.name = "Vasya", .surname = "Ivanov", .age = 21};
    auto tuple = std::make_tuple(student.name, student.surname, student.age);
    std::cout << std::get<0>(tuple) << '\n';

    std::get<0>(tuple) = "Petya";
    std::cout << std::get<0>(tuple) << '\n';
    std::cout << student.name << '\n';

    auto [name, surname, age] = tuple;
    std::cout << name << '\n';


    auto tie = std::tie(student.name, student.surname, student.age);
    std::cout << std::get<0>(tie) << '\n';

    std::get<0>(tie) = "Petya";
    std::cout << std::get<0>(tie) << '\n';
    std::cout << student.name << '\n';



    std::map<char, int> counter;
    std::string s = "quick brown fox jumps over the lazy dog";

    for (char ch : s) {
        ++counter[ch];
    }

    PrintMap(counter);


    size_t i = 0;
    std::cout << i - 1 << '\n';
    std::cout << std::numeric_limits<size_t>::max() << '\n';

    std::vector<int> v;

    if (v.size() - 1 >= 0) {
    if (v.size() >= 1) {
        std::cout << v[v.size() - 1] << '\n';
    } else {
        std::cout << "vector is empty";
    }


    std::vector<int> v = {1, 2, 3, 4, 5};

    for (size_t i = v.size() - 1; i >= 0; --i) {
        std::cout << v[i] << '\n';
    }

    for (size_t i = v.size() - 1; i > -1; --i) {
        std::cout << v[i] << '\n';
    }

    for (size_t i = v.size() - 1; i + 1 > 0; --i) {
        std::cout << v[i] << '\n';
    }

    for (auto it = v.rbegin(); it != v.rend(); ++it) {
        std::cout << *it << '\n';
    }


    std::vector<std::string> v = {"abc", "de", "f"};

    // std::string s
    for (auto s : v) {  // bad
        s[0] = '!';
    }
    PrintVector(v);

    // std::string& s
    for (auto& s : v) {
        s[0] = '!';
    }
    PrintVector(v);

    // const std::string& s
    for (const auto& s : v) {
        std::cout << s.size() << ' ';
    }
    std::cout << '\n';


    int a = 5;
    int* pt = &a;
    *pt = 7;
    std::cout << a << '\n';

    std::cout << *pt << '\n';
    std::cout << pt << '\n';

    ++pt;
    std::cout << *pt << '\n';
    std::cout << pt << '\n';

    std::cout << sizeof(a) << '\n';

    std::cout << a << '\n';


    int a = 5;
    const int* pt = &a;
    *pt = 7;  // doesn't work
    ++pt;

    int a = 5;
    int* const pt = &a;
    *pt = 7;
    ++pt;  // doesn't work

    int a = 5;
    const int* const pt = &a;
    *pt = 7;  // doesn't work
    ++pt;  // doesn't work


    int a = 5;
    const int* pt = &a;
    a = 7;
    std::cout << *pt << '\n';


    int* pt = new int;
    std::cout << *pt << '\n';
    std::cout << pt << '\n';

    *pt = 5;
    std::cout << *pt << '\n';
    std::cout << pt << '\n';

    delete pt;


    return 0;
}