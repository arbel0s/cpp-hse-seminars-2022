#pragma once

#include <iostream>
#include <string>

struct Point {
    double x = 0;
    double y = 0;
};

class Student {
public:
    Student(const std::string& name, int age) : name_(name), age_(age) {}
    explicit Student(int age) : age_(age) {}

    std::string GetName() const {
        return name_;
    }

    void SetName(const std::string& name) {
        name_ = name;
    }

    static void Print(const Student& student) {
        std::cout << "name: " << student.name_ << " age: " << student.age_ << '\n';
    }

    int SomeNontrivialFunction();

    static int AnotherFunction() {
        return 42;
    }

private:
    std::string name_;
    int age_;
};