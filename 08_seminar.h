#include <iostream>
#include <memory>

class Son;
class Daughter;

class Mother {
public:
    ~Mother() {
        std::cout << "Mother is deleted" << '\n';
    }

    void setSon(const std::shared_ptr<Son> son){
        mySon = son;
    }

    void setDaughter(const std::shared_ptr<Daughter> daughter){
        myDaughter = daughter;
    }

private:
    std::weak_ptr<const Son> mySon;
    std::weak_ptr<const Daughter> myDaughter;
};

class Son {
public:
    Son(std::shared_ptr<Mother> mother) : myMother(mother) {}

    ~Son() {
        std::cout << "Son is deleted" << '\n';
    }

private:
    std::shared_ptr<const Mother> myMother;
};

class Daughter {
public:
    Daughter(std::shared_ptr<Mother> mother) : myMother(mother) {}

    ~Daughter() {
        std::cout << "Daughter is deleted" << '\n';
    }

private:
    std::shared_ptr<const Mother> myMother;
};