#include "07_seminar.h"

#include <set>
#include <tuple>
#include <vector>

struct Point {
    double x;
    double y;

    bool operator<(const Point& other) const {
        return std::tie(x, y) < std::tie(other.x, other.y);
    };

//    friend bool operator<(const Point& lhs, const Point& rhs) {
//        return std::tie(lhs.y, lhs.x) < std::tie(rhs.y, rhs.x);
//    };
};

//bool operator<(const Point& lhs, const Point& rhs) {
//    return lhs.x + lhs.y < rhs.x + rhs.y;
//};

std::ostream& operator<<(std::ostream &out, const Point& point){
    out << "(" << point.x << ", " << point.y << ")";
    return out;
}

//void operator<<(std::ostream &out, const Point& point){
//    out << "(" << point.x << ", " << point.y << ")";
//}

int main() {
    Point p1 = {.x = 5, .y = 4};

    std::cout << p1;

    std::cout << p1 << '\n';
    operator<<(std::cout, p1) << '\n';
    operator<<(operator<<(std::cout, p1), '\n');
    operator<<(std::cout, '\n');


    Point p2 = {.x = 7, .y = 1};

    std::cout << (p1 < p2) << '\n';
    std::cout << (p1.operator<(p2)) << '\n';
    std::cout << (operator<(p1, p2)) << '\n';

    std::vector<Point> points = {
        {.x = 5, .y = 3},
        {.x = 2, .y = 7},
        {.x = 4, .y = 2},
        {.x = 8, .y = 6},
        {.x = 2, .y = 5},
    };

    std::sort(points.begin(), points.end());

    for (const auto& point : points) {
        std::cout << point << '\n';
    }


    std::set<Point> points_set = {
        {.x = 5, .y = 3},
        {.x = 2, .y = 7},
        {.x = 4, .y = 2},
        {.x = 8, .y = 6},
        {.x = 2, .y = 5},
    };

    for (const auto& point : points_set) {
        std::cout << point << '\n';
    }


    Integer a(2);
    Integer b(3);

    std::cout << a << ' ' << b << '\n';

    a += b += a;

    std::cout << a << ' ' << b << '\n';


    Integer a(2);
    Integer b(3);

    std::cout << (a == b) << '\n';
    std::cout << (a < b) << '\n';
    std::cout << (a >= b) << '\n';


    Int a(2);
    Int b(a);
    Int c(5);
    c = b;

    std::cout << a << ' ' << b  << ' ' << c << '\n';

    a.SetValue(10);

    std::cout << a << ' ' << b  << ' ' << c << '\n';


    BadIntPtr a(2);
    BadIntPtr b(a);
    BadIntPtr c(5);
    c = b;

    b = b;

    std::cout << a << ' ' << b  << ' ' << c << '\n';

    a.SetValue(10);

    std::cout << a << ' ' << b  << ' ' << c << '\n';


    IntPtr a(2);
    IntPtr b(a);
    IntPtr c(5);
    c = b;

    std::cout << a << ' ' << b  << ' ' << c << '\n';

    a.SetValue(10);

    std::cout << a << ' ' << b  << ' ' << c << '\n';


    return 0;
};
