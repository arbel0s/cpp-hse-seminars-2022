#include "10_seminar.h"

#include <array>
#include <iostream>
#include <map>
#include <string>
#include <type_traits>
#include <vector>


template <typename T>  // template <class T>
T max(const T& lhs, const T& rhs) {
    return (lhs < rhs) ? rhs : lhs;  // rhs if (lhs < rhs) else lhs
}

int main() {
    std::cout << max(5, 7) << '\n';
    std::cout << max(5.5, 7.7) << '\n';

    return 0;
}


template <class T>
T max(const T& lhs, const T& rhs) {
    std::cout << "Template version\n";
    return (lhs < rhs) ? rhs : lhs;
}

double max(const double& lhs, const double& rhs) {
    std::cout << "Non template version\n";
    return (lhs < rhs) ? rhs : lhs;
}

int main() {
    std::cout << max(5, 7) << '\n';
    std::cout << max(5.5, 7.7) << '\n';

    std::cout << max<>(5.5, 7.7) << '\n';

    std::cout << max(5, 7.7) << '\n';

    auto a = max<double>(5, 7);
    std::cout << a << '\n';
    std::cout << std::boolalpha << std::is_same_v<decltype(a), double> << '\n';

    return 0;
}


template <class T1, class T2>
auto max(const T1& lhs, const T2& rhs) {
    return (lhs < rhs) ? rhs : lhs;
}

int main() {
    std::cout << max(5, 7) << '\n';
    std::cout << max(5.5, 7.7) << '\n';

    std::cout << max(5.5, 7) << '\n';

    return 0;
}


template <class T>
void PrintContainer(const T& container) {
    for (const auto& element : container) {
        std::cout << element << ' ';
    }
    std::cout << '\n';
}

int main() {
    std::vector<int> v1 = {1, 2, 3};
    std::vector<std::string> v2 = {"abc", "de", "f"};
    std::string s = "text";

    PrintContainer(v1);
    PrintContainer(v2);
    PrintContainer(s);
}



template <class T, int N>
class Array {
 public:
    int getSize() const {
        return N;
    }

 private:
    T elem[N];
};

int main() {
    Array<int, 5> myArr1;
    Array<int, 10> myArr2;
    Array<int, 5> myArr3;
//    myArr3.getSize();

    return 0;
}


template <typename T, int Rows, int Columns>
class Matrix{
    // ....
};

template <typename T, int Rows>
using Square = Matrix<T, Rows, Rows>;

template <typename T, int Rows>
using Vector = Matrix<T, Rows, 1>;



template <typename T, int Rows, int Column>
class Matrix;

template <typename T>
class Matrix<T, 3, 3>{};

template <>
class Matrix<int, 3, 3>{};


Matrix<int, 3, 3> m1;          // class Matrix<int, 3, 3>

Matrix<double, 3, 3> m2;       // class Matrix<T, 3, 3>

Matrix<std::string, 4, 3> m3;  // class Matrix<T, Line, Column> => ERROR



template <typename... Args>
void variadicTemplate(Args&&... args) {

}


template <typename... Args>
void printSize(Args&&... args){
    std::cout << sizeof...(Args) << ' ';
    std::cout << sizeof...(args) << '\n';
}

int main() {
    printSize();
    printSize("text", 5, 7.7, true);

    return 0;
}


int Multiply() {
    return 1;
}

template<typename T, typename... Ts>
int Multiply(T t, Ts&&... ts) {
    return t * Multiply(ts...);
}


int main() {
    std::cout << Multiply(5, 7) << '\n';
    std::cout << Multiply(1, 2, 3, 4, 5) << '\n';

    return 0;
}
