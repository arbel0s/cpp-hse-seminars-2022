#include <algorithm>
#include <cctype>
#include <iostream>
#include <string>
#include <vector>

void PrintVector(const std::vector<int>& v) {
    for (const auto& element : v) {
        std::cout << element << ' ';
    }
    std::cout << '\n';
}

int main() {
    std::vector<int> v = {2, -3, 5, 0, 4, 5, -1};
    std::string s = "HeLlO WoRlD! :) 1337";

    std::cout << std::any_of(v.begin(), v.end(), [](int x) { return x < 0; }) << '\n';

    std::cout << std::all_of(s.begin(), s.end(), [](char x) { return 33 <= x && x <= 126; }) << '\n';
    std::cout << std::any_of(s.begin(), s.end(), [](char x) { return std::isupper(x); }) << '\n';

    std::cout << std::all_of(s.begin(), s.end(), [](char x) { return std::isalpha(x); }) << '\n';
    std::cout << std::all_of(s.begin(), s.begin() + 5, [](char x) { return std::isalpha(x); }) << '\n';


    std::for_each(v.begin(), v.end(), [](int x) { std::cout << x << ' '; });
    std::cout << '\n';


    std::cout << std::count_if(v.begin(), v.end(), [](int x) { return x > 0; }) << '\n';
    std::cout << std::count(s.begin(), s.end(), ' ') << '\n';


    auto it = std::find(v.begin(), v.end(), 5);
    std::cout << it - v.begin() << '\n';
    std::cout << std::distance(v.begin(), it) << '\n';
    if (it != v.end()) {
      std::cout << *it << '\n';
    } else {
        std::cout << "not found\n";
    }

    auto it = std::find_if(s.begin(), s.end(), [](char x) { return std::isdigit(x); });
    std::cout << it - s.begin() << '\n';
    if (it != s.end()) {
      std::cout << *it << '\n';
    }

    // works with errors
    auto it = s.begin() - 1;
    while (it != s.end()) {
        it = std::find_if(it + 1, s.end(), [](char x) { return std::isdigit(x); });
        std::cout << *it << ' ';
    }


    std::vector<int> v_copy;
    std::copy(v.begin(), v.end(), v_copy.begin());  // doesn't work
    PrintVector(v_copy);

    std::vector<int> v_copy(v.size());
    PrintVector(v_copy);
    std::copy(v.begin(), v.end(), v_copy.begin());  // works, but not good
    PrintVector(v_copy);

    std::vector<int> v_copy;
    std::copy(v.begin(), v.end(), std::back_inserter(v_copy));  // works good
    PrintVector(v_copy);


    std::vector<int> v_copy;
    std::copy_if(v.begin(), v.end(), std::back_inserter(v_copy), [](int x) { return x > 0; });
    PrintVector(v_copy);

    std::string s_copy;
    std::copy_if(s.begin(), s.end(), std::back_inserter(s_copy), [](char x) { return x != ' '; });
    std::cout << s_copy << '\n';


    std::vector<int> v_copy;
    std::transform(v.begin(), v.end(), std::back_inserter(v_copy), [](int x) { return x * x; });
    PrintVector(v_copy);

    std::transform(v.begin(), v.end(), v.begin(), [](int x) { return x * x; });
    PrintVector(v);

    std::string s_copy;
    std::transform(s.begin(), s.end(), std::back_inserter(s_copy), [](char x) { return std::tolower(x); });
    std::cout << s_copy << '\n';


    std::vector<int> v_copy = {0, 1, 2, 0, 3, 0, 4, 0, 0, 5};
    auto it = std::remove(v_copy.begin(), v_copy.end(), 0);
    PrintVector(v_copy);

    std::cout << it - v_copy.begin() << '\n';

    v_copy.erase(it, v_copy.end());
    PrintVector(v_copy);

    std::vector<int> v_copy = {0, 1, 2, 0, 3, 0, 4, 0, 0, 5};
    v_copy.erase(std::remove(v_copy.begin(), v_copy.end(), 0), v_copy.end());
    PrintVector(v_copy);

    std::vector<int> v_copy = {0, 1, 2, 0, 3, 0, 4, 0, 0, 5};
    auto it = std::remove(v_copy.begin(), v_copy.end(), 0);
    std::fill(it, v_copy.end(), 0);
    PrintVector(v_copy);


    return 0;
}
