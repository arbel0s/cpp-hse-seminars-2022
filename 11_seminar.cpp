#include <iostream>
#include <fstream>
#include <bit>

int main(int argc, char* argv[]) {

//    for (size_t i = 0; i < argc; ++i) {
//        std::cout << argv[i] << '\n';
//    }

    std::ifstream input_stream("../example.bmp", std::ios::binary);

    std::cout << std::boolalpha << input_stream.is_open() << '\n';

    uint8_t a;

    a = input_stream.get();

    std::cout << std::hex << static_cast<int>(a) << '\n';


    uint8_t a;
    uint8_t b;

    input_stream.read(reinterpret_cast<char*>(&a), sizeof(a));
    input_stream.read(reinterpret_cast<char*>(&b), sizeof(b));


    std::cout << std::hex << static_cast<int>(a) << '\n';
    std::cout << std::hex << static_cast<int>(b) << '\n';

    input_stream.seekg(0);

    uint16_t c;

    input_stream.read(reinterpret_cast<char*>(&c), sizeof(c));

    std::cout << std::hex << c << '\n';

    uint8_t a;
    uint8_t b;

    input_stream.read(reinterpret_cast<char*>(&a), sizeof(a));
    input_stream.read(reinterpret_cast<char*>(&b), sizeof(b));

    uint16_t result = 0;
    result |= a;
    result |= (b << 8);

    std::cout << std::hex << result << '\n';
    std::cout << std::dec << result << '\n';

    input_stream.seekg(0);

    uint16_t d;
    uint32_t e;

    input_stream.read(reinterpret_cast<char*>(&d), sizeof(d));
    input_stream.read(reinterpret_cast<char*>(&e), sizeof(e));

    std::cout << std::hex << d << '\n';
    std::cout << std::setw(8) << std::setfill('0');
    std::cout << std::hex << e << '\n';

    uint32_t width;
    uint32_t height;

//    input_stream.seekg(18);
    input_stream.seekg(0x12);
    input_stream.read(reinterpret_cast<char*>(&width), sizeof(width));
    input_stream.read(reinterpret_cast<char*>(&height), sizeof(height));

    std::cout << std::dec;
    std::cout << width << '\n';
    std::cout << height << '\n';



    input_stream.seekg(0x12);

    uint32_t width = 0;

    uint8_t a;
    uint8_t b;
    uint8_t c;
    uint8_t d;

    input_stream.read(reinterpret_cast<char*>(&a), sizeof(a));
    input_stream.read(reinterpret_cast<char*>(&b), sizeof(b));
    input_stream.read(reinterpret_cast<char*>(&c), sizeof(c));
    input_stream.read(reinterpret_cast<char*>(&d), sizeof(d));

    std::cout << static_cast<int>(a) << ' ' <<
        static_cast<int>(b) << ' ' <<
        static_cast<int>(c) << ' ' <<
        static_cast<int>(d) << '\n';

    width |= a;
    width |= (b << 8);
    width |= (c << 16);
    width |= (d << 24);

    std::cout << width << '\n';

    struct Color {
        uint8_t red;
        uint8_t green;
        uint8_t blue;
    };

    class Image {

        //...

    private:
        std::vector<std::vector<Color>> data_;
    };



    return 0;
}