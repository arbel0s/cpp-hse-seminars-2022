#include "06_seminar.h"

#include <array>
#include <iostream>
#include <string>
#include <string_view>
#include <vector>
#include <array>

int Student::SomeNontrivialFunction() {
    if (name_ == "Vasya") {
        return 42;
    }
    return 24;
}

int SomeStudentFunction(const Student& student) {
    if (student.GetName() == "Vasya") {
        return 42;
    }
    return 24;
}

std::string_view Good(const std::string& s) {
    std::string_view sv(s);
    return sv;
}

std::string_view Bad() {
    std::string s = "qwerty";
    std::string_view sv(s);
    return sv;
}

std::string Func() {
    std::string s = "qwerty";
    return s;
}

int main() {
    std::string s = "quick brown fox jumps over the lazy dog";
    std::string_view sv(s);

    std::cout << sv << '\n';
    std::cout << sv[0] << '\n';
    std::cout << sv.substr(7, 8) << '\n';
    std::cout << sv.substr(7) << '\n';
    std::cout << s.substr(7, 8) << '\n';
    std::cout << sv.find(' ') << '\n';
    std::cout << sv.find("brown") << '\n';
    std::cout << sv.find("brownskjfhafh") << '\n';
    std::cout << std::string::npos << '\n';
    std::cout << static_cast<size_t>(-1) << '\n';
    if (sv.find("brownskjfhafh") == std::string::npos) {
        std::cout << "not found\n";
    }

    std::cout << Good(s) << '\n';
    std::cout << Bad() << '\n';
    std::cout << Func() << '\n';


    std::vector<int> v = {1, 2, 3};
    std::cout << v[5] << '\n';

    std::array<int, 3> a = {1, 2, 3};
    std::cout << a[5] << '\n';

    int a = std::numeric_limits<int>::max();
    std::cout << a * a << '\n';


    Point p;
    std::cout << p.x << ' ' << p.y << '\n';
    Point q = {.x = 5, .y = 7};
    std::cout << q.x << ' ' << q.y << '\n';

    Student student("Vasya", 21);
    Student::Print(student);
    std::cout << student.GetName() << '\n';
    std::cout << student.SomeNontrivialFunction() << '\n';
    std::cout << student.AnotherFunction() << '\n';
    std::cout << Student::AnotherFunction() << '\n';

    // when Student(int age) constructor is not marked explicit
    Student bad = 5;
    Student::Print(bad);
    std::cout << SomeStudentFunction(5) << '\n';

    Student good(5);
    Student::Print(good);
    std::cout << SomeStudentFunction(Student(5)) << '\n';


    return 0;
}