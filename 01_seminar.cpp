#include <iostream>
#include <string>
#include <vector>
#include <iomanip>

int main() {
    std::cout << "Hello, World!" << std::endl;

    int number = 123456;
    double pi = 3.1415;

    int a;
    std::cin >> a;
    std::cout << a << '\n';

    int a;
    std::cout << a << '\n';

    int a = 7;
    int b = 3;
    std::cout << a / b << '\n';
    std::cout << a % b << '\n';

    std::cout << 1.0 * a / b << '\n';
    std::cout << std::setprecision(30) << static_cast<double>(a) / b << '\n';

    int n = 2147483647;
    std::cout << n + 1 << '\n';

    int32_t n_32 = 2147483647;
    std::cout << static_cast<int64_t>(n_32) + 1 << '\n';

    int64_t n_64 = 2147483647;
    std::cout << n_64 + 1 << '\n';

    long long n_long = 2147483647;
    std::cout << n_long + 1 << '\n';

    size_t a = 0;
    std::cout << a - 1 << '\n';


    int a;
    int b;
    std::cin >> a >> b;
    if (a != b || b == 0) {
        std::cout << "Product of a and b is zero" << '\n';
    } else if ((a > 0 && b > 0) || (a < 0 && b < 0)) {
        std::cout << "Product of a and b is positive" << '\n';
    } else {
        std::cout << "Product of a and b is negative" << '\n';
    }

    if (!(a && b || c)) {
        std::cout << "a is zero" << '\n';
    }

    int a;
    std::cin >> a;
    switch (a) {
        case 1:
            std::cout << "abc\n";
            break;
        case 2:
            std::cout << "cde\n";
            break;
        default:
            std::cout << "qwe\n";
    }


    int i = 0;
    int n = 5;
    while(i < n) {
        std::cout << i << '\n';
        ++i;  // i += 1
    }
    std::cout << '\n';

    int n = 5;
    for (int i = 0; i < n; ++i) {
        std::cout << i << '\n';
    }

    int i = 0;
    ++i;  // good
    i++;  // bad
    std::cout << --i << '\n';


    std::cout << i++ + ++i << '\n';


    char letter = 'a';
    char num_letter = 97;

    std::cout << letter << '\n';
    std::cout << num_letter << '\n';

    char sum = ':' + ')';
    std::cout << sum << '\n';


    std::vector<int> v;
    std::cout << v.size() << '\n';

    std::vector<int> v(5);
    for (size_t i = 0; i < v.size(); ++i) {
        std::cout << v[i] << ' ';
    }

    std::vector<int> v(5, 7);
    for (size_t i = 0; i < v.size(); ++i) {
        std::cout << v[i] << ' ';
    }

    std::vector<int> v = {1, 2, 3, 4, 5};
    for (size_t i = 0; i < v.size(); ++i) {
        std::cout << v[i] << ' ';
    }

    std::vector<int> v = {1, 2, 3, 4, 5};
    std::cout << v.back() << '\n';
    std::cout << v[-1] << '\n';

    std::vector<int> v;
    int a;
    std::cin >> a;
    while (a != 0) {
        v.push_back(a);
        std::cin >> a;
    }
    for (size_t i = 0; i < v.size(); ++i) {
        std::cout << v[i] << ' ';
    }


    std::string s = "qwerty";
    s[3] = 'z';
    for (size_t i = 0; i < s.size(); ++i) {
        std::cout << s[i] << ' ';
    }

    std::string s = "qwerty";
    std::string t = "123456";
    std::cout << s + t << '\n';
    std::cout << s.substr(2, 3) << '\n';

    return 0;
}
