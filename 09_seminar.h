#include <iostream>
#include <string>


class Animal {
public:
    explicit Animal(std::string name) : name_(std::move(name)) {};

    virtual void Say() const {
        std::cout << "Some noises\n";
    }

    virtual void SayName() const {
        std::cout << "My name is " << name_ << "\n";
    }

    virtual int LegsCount() const = 0;

    virtual ~Animal() = default;
//    virtual ~Animal() {};

protected:
    std::string name_;
};

int Animal::LegsCount() const {
    return -1;
}


class Cat : public Animal {
public:
    using Animal::Animal;

    virtual void Say() const override {
        std::cout << "Meow!\n";
    }

    virtual int LegsCount() const override {
        return 4;
    }

    void MurrMurr() const {
        std::cout << "MurrMurr!\n";
    }
};

class GoodBoy {
public:
    virtual void WhoIsAGoodBoy() const {
        std::cout << "Who is a good boy?\n";
    }

    virtual ~GoodBoy() = default;
};

class Dog : public Animal, public GoodBoy {
public:
    using Animal::Animal;

    virtual void Say() const override {
        std::cout << "Woof!\n";
    }

    virtual void WhoIsAGoodBoy() const override {
        GoodBoy::WhoIsAGoodBoy();
        std::cout << name_ << " is a good boy!\n";
    }

    virtual int LegsCount() const override {
        return 4;
    }
};


class ClassA {
public:
    int a;
};

class ClassB : public ClassA {
public:
    int b;
};

class ClassC : public ClassA {
public:
    int c;
};

class ClassD : public ClassB, public ClassC {
public:
    int d;
};


class A {
public:
    int x;
protected:
    int y;
private:
    int z;
};

class B : public A {
    // x is public
    // y is protected
    // z is not accessible from B
};

class C : protected A {
    // x is protected
    // y is protected
    // z is not accessible from C
};

class D : private A {  // 'private' is default for classes
    // x is private
    // y is private
    // z is not accessible from D
};