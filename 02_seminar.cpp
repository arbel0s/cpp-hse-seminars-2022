#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

void PassByValue(std::string s) {
    s[0] = '!';
}

void PassByReference(std::string& s) {
    s[0] = '!';
}

void PassByConstReference(const std::string& s) {
//    s[0] = '!';
    std::cout << s.size() << '\n';
}

void PassByPointer(std::string* s) {
    (*s)[0] = '!';
    std::cout << s->size() << '\n';
}

std::string ModifyString(std::string s) {
    s[0] = '!';
    return s;
}

//    if (s[0] == '1') {
//        return s;
//    } else {
//        return 5;
//    }


void PrintVector(const std::vector<int>& v) {
    for (const auto& element : v) {
        std::cout << element << ' ';
    }
    std::cout << '\n';
}

bool ReverseSort(const int& lhs, const int& rhs) {
    return lhs > rhs;
}

int main() {
    int a = 1;
    int b = a;
    std::cout << a << ' ' << b << '\n';
    b = 2;
    std::cout << a << ' ' << b << '\n';

    int a = 1;
    int& b = a;
    std::cout << a << ' ' << b << '\n';
    a = 2;
    std::cout << a << ' ' << b << '\n';

    const int a = 1;
    a = 2;

    const int& a = 1;
    a = 2;

    int a = 1;
    int* b = &a;
    std::cout << *b << '\n';
    *b = 2;
    std::cout << *b << '\n';


    std::string s = "qwerty";

    PassByValue(s);
    std::cout << s << '\n';

    PassByReference(s);
    std::cout << s << '\n';

    PassByConstReference(s);

    PassByPointer(&s);
    std::cout << s << '\n';


    int a = 1;
    auto b = a;

    std::string s = "qwerty";
    auto new_s = ModifyString(s);
    std::cout << s << ' ' << new_s << '\n';

    auto a = 1;  // bad


    int a;
    std::cin >> a;
    auto b = a < 0 ? -a : a;
    std::cout << b << '\n';

//    int b;
//    if (a < 0) {
//        b = -a;
//    } else {
//        b = a;
//    }

    std::vector<int> v = {2, 6, 3, 0, 4};

    for (auto it = v.begin(); it < v.end(); ++it) {
        std::cout << *it << ' ';
    }
    std::cout << '\n';

    for (size_t i = 0; i < v.size(); ++i) {
        std::cout << v[i] << ' ';
    }

    std::vector<int> v = {2, 6, 3, 0, 4};
    std::sort(v.begin(), v.end());
    PrintVector(v);

    std::sort(v.begin(), v.end(), ReverseSort);
    PrintVector(v);

    std::sort(v.begin(), v.end(), [](int lhs, int rhs) { return lhs > rhs; } );
    PrintVector(v);


    return 0;
}