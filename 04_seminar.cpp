#include <algorithm>
#include <deque>
#include <forward_list>
#include <functional>
#include <iostream>
#include <iterator>
#include <map>
#include <queue>
#include <set>
#include <stack>
#include <unordered_map>
#include <unordered_set>

void PrintSet(const std::set<int>& set) {
//    for (auto it = set.begin(); it != set.end(); ++it) {
//        std::cout << *it << ' ';
//    }

    for (int x : set) {
        std::cout << x << ' ';
    }

    std::cout << '\n';
}

void PrintUnorderedSet(const std::unordered_set<int>& set) {
    for (int x : set) {
        std::cout << x << ' ';
    }
    std::cout << '\n';
}

int main() {
    std::forward_list<int> list = {1, 2, 3, 4, 5};

    auto it = list.begin();
    std::cout << *it << '\n';

    ++it;
    std::cout << *it << '\n';

    it = list.insert_after(it, 10);
    std::cout << *it << '\n';

    ++it;
    list.erase_after(it);

    std::for_each(list.begin(), list.end(), [](int x) { std::cout << x << ' '; });
    std::cout << '\n';


    std::deque<int> d = {1, 2, 3, 4, 5};
    d.pop_back();
    d.push_back(100);
    d.pop_front();
    d.push_front(-100);
    std::for_each(d.begin(), d.end(), [](int x) { std::cout << x << ' '; });

    std::vector<int> v = {1, 2, 3};
    auto it = v.begin();


    std::stack<int> s;
    s.push(1);
    s.push(2);
    std::cout << s.top() << '\n';
    s.pop();
    std::cout << s.top() << '\n';


    std::queue<int> q;
    q.push(1);
    q.push(2);
    std::cout << q.front() << '\n';
    q.pop();
    std::cout << q.front() << '\n';


    std::vector<int> v = {1, 5, 7, 3, 9, 8, 1};
    std::priority_queue<int> q(v.begin(), v.end());

    while (!q.empty()) {
        std::cout << q.top() << ' ';
        q.pop();
    }
    std::cout << '\n';

    std::vector<int> v = {1, 5, 7, 3, 9, 8, 1};
    std::priority_queue<int, std::vector<int>, std::greater<>> q(v.begin(), v.end());

    while (!q.empty()) {
        std::cout << q.top() << ' ';
        q.pop();
    }
    std::cout << '\n';


    std::set<int> s = {1, 5, 7, 2, 4, 4, 1};
    PrintSet(s);

    s.insert(10);
    s.erase(5);
    PrintSet(s);

    std::cout << s.contains(10) << '\n';
    std::cout << (s.find(10) != s.end()) << '\n';


    std::set<int> s = {1, 5, 7, 2, 4, 4, 1};
    PrintSet(s);

    std::set<int> t = {2, 5, 7, 3, 8};
    PrintSet(t);

    std::set<int> i;
    std::set_intersection(s.begin(), s.end(), t.begin(), t.end(), std::inserter(i, i.begin()));
    PrintSet(i);

    std::set<int> u;
    std::set_union(s.begin(), s.end(), t.begin(), t.end(), std::inserter(u, u.begin()));
    PrintSet(u);


    std::unordered_set<int> s = {1, 5, 7, 2, 4, 4, 1};
    PrintUnorderedSet(s);

    s.insert(10);
    s.erase(5);
    PrintUnorderedSet(s);

    std::cout << s.contains(10) << '\n';
    std::cout << (s.find(10) != s.end()) << '\n';


    std::map<std::string, int> map = {{"a", 1}, {"b", 2}, {"c", 3}};

    std::cout << map["b"] << '\n';

    map["d"] = 4;
    std::cout << map["d"] << '\n';

    std::cout << map["e"] << '\n';

    std::cout << map.contains("f") << '\n';  // std::cout << (map.find("f") != map.end()) << '\n';
    map["f"];
    std::cout << map.contains("f") << '\n';

    std::cout << '\n';

    std::pair<std::string, int> p = {"a", 1};
    p.first = "a";
    p.second = 1;
    std::cout << p.first << ' ' << p.second << '\n';


    auto it = map.begin();
    std::cout << (*it).first << ' ' << (*it).second << '\n';
    std::cout << it->first << ' ' << it->second << '\n';

    for (auto it = map.begin(); it != map.end(); ++it) {
        std::cout << it->first << ' ' << it->second << '\n';
    }

    std::cout << '\n';

    for (const auto& pair : map) {
        std::cout << pair.first << ' ' << pair.second << '\n';
    }

    std::cout << '\n';

    for (const auto& [key, value] : map) {
        std::cout << key << ' ' << value << '\n';
    }

    return 0;
}
