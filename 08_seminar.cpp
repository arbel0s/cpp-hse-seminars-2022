#include "08_seminar.h"

#include <exception>
#include <iostream>
#include <memory>
#include <utility>


//void func(std::unique_ptr<int> u_ptr);
//func(std::move(u_ptr));

//void func(std::unique_ptr<int>& u_ptr);
//func(u_ptr);

//void func(const std::unique_ptr<int>& u_ptr); // bad
//void func(int i);
//func(*u_ptr);

int main(){
    int* ptr = new int;
    *ptr = 5;
    std::unique_ptr<int> u_ptr(ptr);

    std::cout << ptr << '\n';
    std::cout << u_ptr.get() << '\n';

    std::cout << *ptr << '\n';
    std::cout << *u_ptr << '\n';


    std::unique_ptr<int> u_ptr(new int(5));

    std::unique_ptr<int> u_ptr = std::make_unique<int>(5);

    auto u_ptr = std::make_unique<int>(5);


    if (u_ptr != nullptr) {
        std::cout << "contained value: " << *u_ptr << '\n';
    } else {
        std::cout << "pointer is empty\n";
    }

    std::unique_ptr<int> u_ptr_2;

    if (u_ptr_2) {
        std::cout << "contained value: " << *u_ptr_2 << '\n';
    } else {
        std::cout << "pointer is empty\n";
    }


    auto u_ptr = std::make_unique<int>(5);
    std::cout << u_ptr.get() << '\n';

    int* ptr = u_ptr.release();

    std::cout << *ptr << '\n';

    std::cout << ptr << '\n';
    std::cout << u_ptr.get() << '\n';

    delete ptr;


    auto u_ptr = std::make_unique<int>(5);

    u_ptr.reset(new int(7));

    std::cout << *u_ptr << '\n';


    auto u_ptr = std::make_unique<int>(5);
    auto u_ptr_2 = std::make_unique<int>(7);
    std::unique_ptr<int> u_ptr_2;

    std::cout << u_ptr.get() << '\n';
    std::cout << u_ptr_2.get() << '\n';

    // doesn't work
    u_ptr_2 = u_ptr;

    u_ptr_2 = std::move(u_ptr);

    std::cout << u_ptr.get() << '\n';
    std::cout << u_ptr_2.get() << '\n';


    auto u_ptr = std::make_unique<int>(5);
    auto u_ptr_2 = std::make_unique<int>(7);

    std::cout << u_ptr.get() << '\n';
    std::cout << u_ptr_2.get() << '\n';

    std::swap(u_ptr, u_ptr_2);

    std::cout << u_ptr.get() << '\n';
    std::cout << u_ptr_2.get() << '\n';



    std::shared_ptr<int> sh_ptr(new int(5));

    auto sh_ptr = std::make_shared<int>(5);

    std::cout << sh_ptr.get() << '\n';
    std::cout << *sh_ptr << '\n';


    auto sh_ptr = std::make_shared<int>(5);

    std::cout << sh_ptr.use_count() << '\n';

    auto sh_ptr_2(sh_ptr);

    std::cout << sh_ptr.use_count() << '\n';

    auto sh_ptr_3 = sh_ptr_2;

    std::cout << sh_ptr.use_count() << '\n';

    {
        std::shared_ptr<int> sh_ptr_4;
        sh_ptr_4 = sh_ptr;
        std::cout << sh_ptr.use_count() << '\n';
    }

    std::cout << sh_ptr.use_count() << '\n';


    std::weak_ptr<int> w_ptr;

    {
        auto sh_ptr = std::make_shared<int>(5);

        std::cout << "use_count: " << sh_ptr.use_count() << '\n';

        w_ptr = sh_ptr;

        std::cout << "use_count: " << sh_ptr.use_count() << '\n';

        if (w_ptr.expired()) {
            std::cout << "ptr is expired\n";
        } else {
            std::shared_ptr<int> w_ptr_locked = w_ptr.lock();
            std::cout << "value: " << *w_ptr_locked << '\n';
            std::cout << "use_count: " << sh_ptr.use_count() << '\n';
        }
    }

    if (w_ptr.expired()) {
        std::cout << "ptr is expired\n";
    } else {
        std::shared_ptr<int> w_ptr_locked = w_ptr.lock();
        std::cout << "value: " << *w_ptr_locked << '\n';
        std::cout << "use_count: " << w_ptr_locked.use_count() << '\n';
    }


    if (std::shared_ptr<int> w_ptr_locked = w_ptr.lock()) {
        std::cout << "value: " << *w_ptr_locked << '\n';
        std::cout << "use_count: " << w_ptr_locked.use_count() << '\n';
    } else {
        std::cout << "ptr is expired\n";
    }

    {
        auto mother = std::make_shared<Mother>();
        auto son = std::make_shared<Son>(mother);
        auto daughter = std::make_shared<Daughter>(mother);
        mother->setSon(son);
        mother->setDaughter(daughter);

        std::cout << "mother: " << mother.use_count() << '\n';
        std::cout << "son: " << son.use_count() << '\n';
        std::cout << "daughter: " << daughter.use_count() << '\n';
    }

//    std::cout << "mother: " << mother.use_count() << '\n';
//    std::cout << "son: " << son.use_count() << '\n';
//    std::cout << "daughter: " << daughter.use_count() << '\n';





//    try {
//        throw std::runtime_error("some error text");
//    } catch(const std::exception& e) {
//        std::cout << e.what();
//    }



    return 0;
}