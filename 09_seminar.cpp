#include "09_seminar.h"

#include <iostream>
#include <memory>
#include <vector>


int main() {
    Animal* cat = new Cat("Tom");

    cat->Say();

    delete cat;


    std::unique_ptr<Animal> cat = std::make_unique<Cat>("Tom");

    cat->Say();

    cat->SayName();

    cat->Animal::Say();

    std::cout << cat->LegsCount() << '\n';
    std::cout << cat->Animal::LegsCount() << '\n';


    std::unique_ptr<Dog> dog = std::make_unique<Dog>("Sharik");
    dog->WhoIsAGoodBoy();
    dog->Say();


    std::vector<std::unique_ptr<Animal>> animals;

    animals.push_back(std::make_unique<Cat>("Tom"));
    animals.push_back(std::make_unique<Dog>("Sharik"));

    for (const auto& animal : animals) {
        animal->SayName();
        animal->Say();

        if (Cat* cat = dynamic_cast<Cat*>(animal.get())) {
            std::cout << "It's a cat!\n";
            cat->MurrMurr();
        } else if (Dog* dog = dynamic_cast<Dog*>(animal.get())) {
            std::cout << "It's a dog!\n";
            dog->WhoIsAGoodBoy();
        } else {
            std::cout << "Animal is unrecognized\n";
        }
    }



    ClassD obj;

//     obj.a = 10;  // Error

    obj.ClassB::a = 10;
    obj.ClassC::a = 100;

    obj.b = 20;
    obj.c = 30;
    obj.d = 40;

    std::cout << "a from ClassB  : " << obj.ClassB::a << '\n';
    std::cout << "a from ClassC  : " << obj.ClassC::a << '\n';

    std::cout << "b : " << obj.b << '\n';
    std::cout << "c : " << obj.c << '\n';
    std::cout << "d : " << obj.d << '\n' << '\n';


    return 0;
}