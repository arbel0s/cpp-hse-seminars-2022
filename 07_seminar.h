#pragma once

#include <algorithm>
#include <compare>
#include <iostream>
#include <string>

class Integer {
private:
    int value_;

public:
    explicit Integer(int value): value_(value) {}

    friend const Integer operator+(const Integer& lhs, const Integer& rhs) {
        return Integer(lhs.value_ + rhs.value_);
    }

    friend Integer& operator+=(Integer& lhs, const Integer& rhs) {
        lhs.value_ += rhs.value_;
        return lhs;
    }

    // унарный минус
    friend const Integer operator-(const Integer& i) {
        return Integer(-i.value_);
    }

    //префиксный инкремент ++i
    friend const Integer& operator++(Integer& i) {
        ++i.value_;
        return i;
    }

    //постфиксный инкремент i++
    friend const Integer operator++(Integer& i, int) {
        Integer oldValue(i.value_);
        ++i.value_;
        return oldValue;
    }

    friend std::ostream& operator<<(std::ostream &out, const Integer& i) {
        out << i.value_;
        return out;
    }

//    friend bool operator==(const Integer& lhs, const Integer& rhs) {
//        return lhs.value_ == rhs.value_;
//    }
//
//    friend bool operator<(const Integer& lhs, const Integer& rhs) {
//        return lhs.value_ < rhs.value_;
//    }

    // ...

    // starship operator
    auto operator<=>(const Integer& other) const = default;

//    auto operator<=>(const Integer& other) const {
//        return value_ <=> other.value_;
//    }

//    auto operator<=>(const Integer& other) const {
//        if (value_ < other.value_) {
//            return -1;
//        } else if (value_ > other.value_) {
//            return 1;
//        } else {
//            return 0;
//        }
//    }
};

class Int {
public:
    explicit Int(int value) : value_(value) {}

    void SetValue(int value) {
        value_ = value;
    }

    friend std::ostream& operator<<(std::ostream &out, const Int& i) {
        out << i.value_;
        return out;
    }

private:
    int value_;
};


class BadIntPtr {
public:
    explicit BadIntPtr(int value) {
        ptr_ = new int;
        *ptr_ = value;
    }

    ~BadIntPtr() {
        delete ptr_;
    }

    void SetValue(int value) {
        *ptr_ = value;
    }

    friend std::ostream& operator<<(std::ostream &out, const BadIntPtr& i) {
        out << *(i.ptr_);
        return out;
    }

private:
    int* ptr_;
};


class IntPtr {
 private:
    int* ptr_;

 public:
    explicit IntPtr(int value) {
        ptr_ = new int;
        *ptr_ = value;
    }

    ~IntPtr() {
        delete ptr_;
    }

    IntPtr(const IntPtr& other) {
        ptr_ = new int;
        *ptr_ = *(other.ptr_);
    }

    // Bad version #1
//    IntPtr& operator=(const IntPtr& other) {
//        delete ptr_;
//        ptr_ = new int;
//        *ptr_ = *(other.ptr_);
//        return *this;
//    }

    // Bad version #2
//    IntPtr& operator=(const IntPtr& other) {
//        if (this != &other) {
//            delete ptr_;
//            ptr_ = new int;
//            *ptr_ = *(other.ptr_);
//        }
//
//        return *this;
//    }

    // copy-and-swap idiom
    friend void swap(IntPtr& lhs, IntPtr& rhs) {
        std::swap(lhs.ptr_, rhs.ptr_);
    }

    // Ok version
//    IntPtr& operator=(const IntPtr& other) {
//        IntPtr tmp(other);
//        swap(*this, tmp);
//        return *this;
//    }

    // Good version (finally)
    IntPtr& operator=(IntPtr other) {
        swap(*this, other);
        return *this;
    }

    void SetValue(int value) {
        *ptr_ = value;
    }

    friend std::ostream& operator<<(std::ostream &out, const IntPtr& i) {
        out << *(i.ptr_);
        return out;
    }
};
